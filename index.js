console.log('hello');

// ARRAY METHODS

/*
	1.Mutator Methods
		Mutator methods are functions that mutate or change an array after theyr created. These method manipulate the original array performing various tasks such as adding and removing elements.

*/

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];


/*
	push()
		add an element in the end of an array and returns the array length


		syntax:
			arrayName.push(element)

*/

console.log("Current Array:")
console.log(fruits)


// Adding element
let fruitsLength = fruits.push("Mango");
console.log("Mutated array from push method:")
console.log(fruits);


fruits.push("Avocado", "Guava");
console.log(fruits)

/*
	pop()
		Removes the last element in our array AND returns the removed element

		Syntax:
			arrayName.pop()


*/

let removedFruit = fruits.pop()
console.log(removedFruit)
console.log("Mutated array from the pop method:");
console.log(fruits);

/*
	unshift()
		Adds one or more elements at the beginning of an array

	Syntax:
		arrayName.unshift(elementA)
		arrayName.unshift(elementA, elementB)

*/

fruits.unshift("Lime", "Banana");

let unshiftMethod = fruits.unshift("Lime", "Banana")
console.log("Mutated array from unshift method:");
console.log(fruits)
console.log(unshiftMethod)


/*
	shift()
		Remove an element at the beginning of our array AND returns the remove element

		Syntax:
			arrayName.shift()


*/

let anotherFruit = fruits.shift()
console.log(anotherFruit);
console.log("Mutated array from shift method: ");
console.log(fruits);

/*
	splice()
		Simultaneously removes element from a specified index number and adds an element

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)


*/
console.log(fruits)
let fruitSplice = fruits.splice(1, 4, "Lime", "Cherry")
console.log(fruitSplice)
console.log("Mutated array from splice method:")
console.log(fruits)


/*
	sort()
		rearranges the array elements in alphanumeric order

	Syntax:
		arrayName.sort();
*/

fruits.sort()
console.log("Mutated array from sort method");
console.log(fruits);


let mixedArray = [50, 14, 100, "Carlos", "Nej", "Bernard", "Charles"];
console.log(mixedArray.sort())

/*
For sorting the items in descending order
	fruits.sort().reverse()
	console.log(fruits)


*/


/*
	reverse()
		reverses the order of array elements

	Syntax:
		arrayName.reverse();

*/

fruits.reverse()
console.log("Mutated array from reverse method:")
console.log(fruits)


/*
	Non-mutator Methods
		-These are functions that do not modify or change an array after they are created. These methods do not manipulate the original array performing various task such as returning element from an array and combining arrays and printing the output.
		

*/

let countries = ["US", "PH", "CA", "SG", "TH", "PH", "FR", "DE"];

/*
	indexOf()
		Returns the index number of the first matching element found in an array. If no match was found, the result will be -1. The search process will be done from first element proceeding to the last element.

	Syntax:
		arrayName.indexOf(searchValue)
		arrayName.indexOf(searchValue, fromIndex)
*/

let firstIndex = countries.indexOf("PH", 4);
// let firstIndex1 = countries.indexOf("PH", -1);

console.log("Result of indexOf method: " + firstIndex);
// console.log(firstIndex1);


/*
	lastIndexOf()
		Returns the index number of the last matching element found in an array. The search process will be done from the last element proceeding to the first element

		Syntax:
			arrayName.lastIndexOf(searchValue)
			arrayName.lastIndexOf(searchValue, fromIndex)

*/

let lastIndex = countries.lastIndexOf("PH");
console.log('Result of lastIndexOf: ' + lastIndex)

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf: " +lastIndexStart);


/*

	slice()
		Portions/slices elements from our array AND returns a new array

	Syntax:
		arrayName.slice(startIndex)
		arrayName.slice(startIndex, endingIndex)	


*/

console.log(countries);
let slicedArrayA = countries.slice(2);
console.log("Result from slice method:")
console.log(slicedArrayA)
// console.log(countries)

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:");
console.log(slicedArrayB)

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:")
console.log(slicedArrayC)


/*
	toString()
		Returns an array as a string separated by commas

	Syntax: 
		arrayName.toString()

*/

let stringArray = countries.toString()
console.log("Result from toString method:")
console.log(stringArray);

/*

	concat()
		Combines two arrays and returns the combined results

	Syntax:
		arrayA.concat(arrayB)
		arratA.concat(elementA)

*/

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breath sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method");
console.log(tasks);

// Combining multiple arrays
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// Combining array with elements
let combinedTasks = taskArrayA.concat("smell express", "throw react")
console.log(combinedTasks)


/*
	join()
		Returns an array as a string seperator by specified seperator string

		Syntax:
			arrayName.join(separatorString)

*/

let students = ["Tristan", "Bernard", "Carlos", "Nehemiah"];
console.log(students.join());
console.log(students.join(' '));
console.log(students.join(" - "));


/*
	Iteration Methods are loops designed to perform repetitive tasks on arrays. Useful for manipulating array data resulting in complex tasks

*/

/*
	forEach()
		similar to for loop that iterates on each array element

	Syntax:
		arrayName.forEach( function(indivElement) {
			statement
		})

*/

allTasks.forEach(function(task) {
	console.log(task)
})

// console.clear();

let filteredTasks = [];

allTasks.forEach(function(task) {
	console.log(task)
	if(task.length > 10) {
		filteredTasks.push(task)
	}
})

console.log(allTasks);
console.log("Result of filteredTasks:");
console.log(filteredTasks);

/*

	map()
		Iterates on each element AND returns new array with different values depending on the result of the function's operation

	Syntax:
		let/const resultArray = arrayName.map(function(indivElement){
			statement
		})


*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
	console.log(numbers)
	return number * number
})

console.log('original array:')
console.log(numbers)
console.log("result of map method: ")
console.log(numberMap)


/*
	every()
		checks if all elements in an array met the given condition
		returns a true value if all elements meet the condition and false if otherwise

	Syntax:
		let/const resultArray = arrayName.every(function(indivElement){
			return expression/condition
		})

*/

let allValid = numbers.every(function(number){
	return (number < 3);
})

// let numbers = [1, 2, 3, 4, 5]
console.log("result of every method");
console.log(allValid);

/*
	some()
		checks if at least one element in the array meets the given condition
		Returns a true value if at least one element meets the given condition and false if otherwise

	Syntax:
		let/const resultArray = arrayName.some(function(indivElement){
			return expression/condition
		})

*/

let someValid = numbers.some(function(number){
	return (number < 2)

});

console.log("result of some method:");
console.log(someValid);

/*
	filter()
		Returns new array that contains elements which meets the given condition. return an empty array if no elements were found

	Syntax:
		let/const resultArray = arrayName.filter(function(indivElement){
			return expression/condition
		})


*/

let filterValid = numbers.filter(function(number){
		return (number < 3)
})

console.log("Result of filter method:")
console.log(filterValid)

let nothingFound = numbers.filter(function(number){
		return (number == 0)
})

console.log("result of filter method:")
console.log(nothingFound)

// Filtering using forEach

let filteredNumbers = [];

numbers.forEach(function(number){

	if(number < 3) {
		filteredNumbers.push(number)
	}
})

console.log('Result of the filtering using forEach:')
console.log(filteredNumbers)

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]

/*
	includes method
		methods can be chained using them one after another
		the result of the first method is used on the second method until all chained methods have been resolved.
*/

let filteredProducts = products.filter(function(product){
		return product.toLowerCase().includes("a")
})

console.log("includes "+filteredProducts)

